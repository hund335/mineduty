package mc.hund35.md;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import mc.hund35.md.events.join;
import mc.hund35.md.events.leave;
import mc.hund35.md.gamemodes.TDM;
import mc.hund35.md.tasks.gametime;
import mc.hund35.md.tasks.levelbar;
import mc.hund35.md.tasks.mapswitcher;
import mc.hund35.md.tasks.scoreboard;

public class main
  extends JavaPlugin
  implements Listener
{
  public static main instance;
  
  @SuppressWarnings("deprecation")
public void onEnable()
  {
    registerCommand();
    
    instance = this;
    
    getServer().getPluginManager().registerEvents(this, this);
    Bukkit.getServer().getPluginManager().registerEvents(new join(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new leave(), this);
    /*
    Bukkit.getServer().getPluginManager().registerEvents(new TDM(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new chat(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new falldamage(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new hunger(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new build(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new permissions(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new gunmenu(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new antia(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new slot1(), this);
    */
    String[] Player = { "perm.1", "perm.2", "perm.3" };
    String[] Mod = { "perm.4", "perm.5", "perm.6" };
    String[] Admin = { "perm.7", "perm.8", "perm.9" };
    
    File file = new File(instance.getDataFolder().getPath(), "data/ranks.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    if (!file.exists())
    {
      if (config.getConfigurationSection("Ranks.Player") == null)
      {
        config.createSection("Ranks");
        config.createSection("Ranks.Player");
        config.set("Ranks.Player.Perms", Player);
        config.set("Ranks.Player.Prefix", "");
      }
      if (config.getConfigurationSection("Ranks.Mod") == null)
      {
        config.createSection("Ranks.Mod");
        config.set("Ranks.Mod.Perms", Mod);
        config.set("Ranks.Mod.Prefix", "§8[§dMod§8] ");
      }
      if (config.getConfigurationSection("Ranks.Admin") == null)
      {
        config.createSection("Ranks.Admin");
        config.set("Ranks.Admin.Perms", Admin);
        config.set("Ranks.Admin.Prefix", "§8[§4Admin§8] ");
      }
    }
    try
    {
      config.save(file);
    }
    catch (IOException e1)
    {
      e1.printStackTrace();
    }
    getServer().getScheduler().scheduleSyncRepeatingTask(this, new BukkitRunnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getOnlinePlayers())
        {
          levelbar.displayLevel(p);
        }
      }
    }, 0L, 1L);
    
    getServer().getScheduler().scheduleSyncRepeatingTask(this, new BukkitRunnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getOnlinePlayers())
        {
       scoreboard.Display(p);
        }
      }
    }, 0L, 120L);
    
    
    getServer().getScheduler().scheduleSyncRepeatingTask(this, new BukkitRunnable()
    {
      public void run()
      {
    	  mapswitcher.MapSwitch();
        for (Player p : Bukkit.getOnlinePlayers())
        {
          TDM.teamFinder(p);
          TDM.loadGame(p);
          TDM.endGame(p);
        }
       gametime.Timer();
          
      }
    }, 0L, 20L);
    
  }
  
  private void registerCommand()
  {
    //getCommand("vote").setExecutor(new vote());
  }
  
 
  
  
}
