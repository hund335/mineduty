package mc.hund35.md.tasks;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import mc.hund35.md.main;
import mc.hund35.md.listeners.scoreboards;

public class scoreboard {

	public static void Display(Player p){
		   File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
	          YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
	          if (p.getWorld().getName().equals("world")) {
	        	  scoreboards.LobbyScorebord(p);
	          }
	          if ((p.getWorld().getName().equals("games")) && (config.getString("Game.Gamemode").equals("TDM")) && (config.getBoolean("Game.Game"))) {
	            scoreboards.TDMScorebord(p);
	          }
	}
	
}
