package mc.hund35.md.tasks;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

import mc.hund35.md.main;

public class gametime {

	public static void Timer(){
		 File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
	        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
	        if (config.getBoolean("Game.Game") == true && config.getInt("Game.GameTime") < 241) {
	        
	            config.set("Game.GameTime", Integer.valueOf(config.getInt("Game.GameTime") - 1));
	            try
	            {
	              config.save(file);
	            }
	            catch (IOException e)
	            {
	              e.printStackTrace();
	            }
	            if (config.getInt("Config.GameTime") == 2)
	            {
	              config.set("Game.Red", 0);
	              config.set("Game.Blue", 0);
	              config.set("Game.Players", 0);
	              config.set("Game.Game", false);
	              try
	              {
	                config.save(file);
	              }
	              catch (IOException e)
	              {
	                e.printStackTrace();
	              }
	              config.set("Game.GameTime", 0);
	              config.set("Game.Time", 0);
	              try
	              {
	                config.save(file);
	              }
	              catch (IOException e)
	              {
	                e.printStackTrace();
	              }
	            }
	        }
	}
}
