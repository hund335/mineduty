package mc.hund35.md.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.hund35.md.main;

public class join implements Listener {

	 @EventHandler
	  public void join(PlayerJoinEvent event)
	  {
	    Player p = event.getPlayer();
	    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
	    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
	    this.lobbyItems(p);
	    if (config2.getString("Player." + p.getName() + ".Team") == null)
	    {
	      config2.set("Player." + p.getName() + ".Team", "§cnon");
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	    if (config2.getString("Player." + p.getName() + ".Slot1") == null)
	    {
	      config2.set("Player." + p.getName() + ".Slot1", "M9A11");
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	    if (config2.getString("Player." + p.getName() + ".Level") == null)
	    {
	      config2.set("Player." + p.getName() + ".Level", 1);
	      config2.set("Player." + p.getName() + ".Tag", "PVT");
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	    if (config2.getString("Player." + p.getName() + ".Slot2") == null)
	    {
	      config2.set("Player." + p.getName() + ".Slot2", "§cnon");
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	  }
	 
	 public static void lobbyItems(Player p){
	    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
	    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
	    ItemStack menu = new ItemStack(Material.NETHER_STAR, 1);
	    ItemMeta meta = menu.getItemMeta();
	    meta.setDisplayName("§eGun menu");
	    menu.setItemMeta(meta);
	    p.getInventory().setItem(0, menu);
	    p.updateInventory();
	    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
	    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
	    if ((config.getBoolean("Config.Game") == false) && (p.getWorld().getName().equals("games")) && 
	      (config2.getString("Player." + p.getName() + ".Team").equals("§cnon")))
	    {
	      Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvtp " + p.getName() + " world");
	      p.getInventory().clear();
	      ItemStack air = new ItemStack(Material.AIR);
	      p.getInventory().setHelmet(air);
	      p.getInventory().setChestplate(air);
	      p.getInventory().setLeggings(air);
	      p.getInventory().setBoots(air);
	      p.updateInventory();
	      ItemStack menu2 = new ItemStack(Material.NETHER_STAR, 1);
	      ItemMeta meta2 = menu2.getItemMeta();
	      meta2.setDisplayName("§eGun menu");
	      menu2.setItemMeta(meta2);
	      p.getInventory().setItem(0, menu2);
	      p.updateInventory();
	    }
	 }
	
}
