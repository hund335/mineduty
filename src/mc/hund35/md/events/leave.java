package mc.hund35.md.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.hund35.md.main;

public class leave implements Listener {

	@EventHandler
	  public void leave(PlayerQuitEvent event)
	  {
	    Player p = event.getPlayer();
	    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
	    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
	    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
	    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
	    if (config2.getString("Player." + p.getName() + ".Team").equals("Red"))
	    {
	      config.set("Game.Red", Integer.valueOf(config.getInt("Game.Red") - 1));
	      config.set("Game.Players", Integer.valueOf(config.getInt("Game.Players") - 1));
	      try
	      {
	        config.save(file);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	      config2.set("Player." + p.getName() + ".Team", "§cnon");
	      config2.set("Player." + p.getName() + ".GKills", 0);
	      config2.set("Player." + p.getName() + ".GDeaths", 0);
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	    if (config2.getString("Player." + p.getName() + ".Team").equals("Blue"))
	    {
	      config.set("Game.Blue", Integer.valueOf(config.getInt("Game.Blue") - 1));
	      config.set("Game.Players", Integer.valueOf(config.getInt("Game.Players") - 1));
	      try
	      {
	        config.save(file);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	      config2.set("Player." + p.getName() + ".Team", "§cnon");
	      config2.set("Player." + p.getName() + ".GKills", 0);
	      config2.set("Player." + p.getName() + ".GDeaths", 0);
	      try
	      {
	        config2.save(file2);
	      }
	      catch (IOException e)
	      {
	        e.printStackTrace();
	      }
	    }
	  }
	
}
