package mc.hund35.md.api;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class action
{
  public static void playAction(Player player, String message)
  {
    CraftPlayer p = (CraftPlayer)player;
    IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
    
    PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
    p.getHandle().playerConnection.sendPacket(ppoc);
  }
  
  public static void broadcastAllAction(String message)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
      playAction(p, message);
    }
  }
}
