package mc.hund35.md.listeners;

import java.io.File;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mc.hund35.md.main;

public class guns {

	
	public static void Slot1(Player p)
	  {
	    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
	    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
	    if (config2.getString("Player." + p.getName() + ".Slot1").equals("M9A11"))
	    {
	      ItemStack gun1 = new ItemStack(Material.WOOD_SPADE, 1);
	      p.getInventory().setItem(1, gun1);
	      ItemStack ammo1 = new ItemStack(Material.CLAY_BALL, 64);
	      p.getInventory().setItem(17, ammo1);
	      p.updateInventory();
	    }
	    else if (config2.getString("Player." + p.getName() + ".Slot2").equals("SPAS12"))
	    {
	      ItemStack gun1 = new ItemStack(Material.WOOD_PICKAXE, 1);
	      p.getInventory().setItem(2, gun1);
	      ItemStack ammo1 = new ItemStack(Material.SEEDS, 32);
	      p.getInventory().setItem(16, ammo1);
	      p.updateInventory();
	    }
	  }
	
}
