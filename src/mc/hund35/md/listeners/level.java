package mc.hund35.md.listeners;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import mc.hund35.md.main;
import mc.hund35.md.api.action;

public class level
{
  public static void LevelSystem(Player p)
  {
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    if (config.getInt("Player." + p.getName() + ".Level") == 1)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e250 §8(§6Level 2§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 250)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §62§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 2);
        config.set("Player" + p.getName() + ".Tag", "PV2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 2)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e400 §8(§6Level 3§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 400)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §63§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 3);
        config.set("Player" + p.getName() + ".Tag", "PV3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 3)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e500 §8(§6Level 4§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §64§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 4);
        config.set("Player" + p.getName() + ".Tag", "PFC");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 4)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e750 §8(§6Level 5§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 750)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §65§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 5);
        config.set("Player" + p.getName() + ".Tag", "PF2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 5)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e1000 §8(§6Level 6§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 1000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §66§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 6);
        config.set("Player" + p.getName() + ".Tag", "PF3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 6)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e1250 §8(§6Level 7§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 1250)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §67§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 7);
        config.set("Player" + p.getName() + ".Tag", "SPC");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 7)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e1550 §8(§6Level 8§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 1550)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §68§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 8);
        config.set("Player" + p.getName() + ".Tag", "SP2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 8)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e1700 §8(§6Level 9§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 1700)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §69§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 9);
        config.set("Player" + p.getName() + ".Tag", "SP3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 9)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e2150 §8(§6Level 10§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 2150)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §610§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 10);
        config.set("Player" + p.getName() + ".Tag", "CPL");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 10)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e2500 §8(§6Level 11§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 2500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §611§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 11);
        config.set("Player" + p.getName() + ".Tag", "CP2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 11)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e2750 §8(§6Level 12§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 2750)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §612§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 12);
        config.set("Player" + p.getName() + ".Tag", "CP3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 12)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e3200 §8(§6Level 13§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 3200)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §613§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 13);
        config.set("Player" + p.getName() + ".Tag", "SGT");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 13)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e4500 §8(§6Level 14§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 4500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §614§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 14);
        config.set("Player" + p.getName() + ".Tag", "SG2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 14)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e5500 §8(§6Level 15§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 5500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §615§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 15);
        config.set("Player" + p.getName() + ".Tag", "SG3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 15)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e7500 §8(§6Level 16§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 7500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §616§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 16);
        config.set("Player" + p.getName() + ".Tag", "SSG");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 16)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e9000 §8(§6Level 17§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 9000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §617§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 17);
        config.set("Player" + p.getName() + ".Tag", "SS2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 17)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e13000 §8(§6Level 18§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 13000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §618§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 18);
        config.set("Player" + p.getName() + ".Tag", "SS3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 18)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e15000 §8(§6Level 19§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 15000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §619§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 19);
        config.set("Player" + p.getName() + ".Tag", "SFC");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 19)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e19500 §8(§6Level 20§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 19500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §620§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 20);
        config.set("Player" + p.getName() + ".Tag", "SF2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 20)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e23000 §8(§6Level 21§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 23000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §621§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 21);
        config.set("Player" + p.getName() + ".Tag", "SF3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 21)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e26500 §8(§6Level 22§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 26500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §622§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 22);
        config.set("Player" + p.getName() + ".Tag", "SGM");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 22)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e29500 §8(§6Level 23§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 29500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §623§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 23);
        config.set("Player" + p.getName() + ".Tag", "LT1");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 23)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e34500 §8(§6Level 24§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 34500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §624§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 24);
        config.set("Player" + p.getName() + ".Tag", "LT2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 24)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e37000 §8(§6Level 25§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 37000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §625§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 25);
        config.set("Player" + p.getName() + ".Tag", "LT3");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 25)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e39000 §8(§6Level 26§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 39000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §626§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 26);
        config.set("Player" + p.getName() + ".Tag", "MJR");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 26)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e42000 §8(§6Level 27§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 42000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §627§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 27);
        config.set("Player" + p.getName() + ".Tag", "MJ2");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 27)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e47500 §8(§6Level 28§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 47500)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §628§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 28);
        config.set("Player" + p.getName() + ".Tag", "CPT");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 28)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e50000 §8(§6Level 29§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 50000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §629§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 29);
        config.set("Player" + p.getName() + ".Tag", "GEN");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 29)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§e55000 §8(§6Level 30§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      if (config.getInt("Player." + p.getName() + ".XP") >= 55000)
      {
        p.sendMessage("§6§lLEVEL §8§l» §eYou are now level §630§e!");
        config.set("Player" + p.getName() + ".XP", 0);
        config.set("Player" + p.getName() + ".Level", 30);
        config.set("Player" + p.getName() + ".Tag", "CDR");
        try
        {
          config.save(file);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    if (config.getInt("Player." + p.getName() + ".Level") == 30)
    {
      action.playAction(p, "§7Level system: §e" + config.getInt("Player." + p.getName() + ".XP") + "§8/§cComing Soon §8(§cComing Soon§8)");
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
    }
  }
}
