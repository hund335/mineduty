package mc.hund35.md.listeners;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import mc.hund35.md.main;

public class scoreboards
{
  public static void LobbyScorebord(Player p)
  {
    Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
    Objective ob = board.registerNewObjective("sb", "dummy");
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    
    ob.setDisplaySlot(DisplaySlot.SIDEBAR);
    
    ob.setDisplayName("§e§nLobby");
    
    ob.getScore("§0").setScore(8);
    ob.getScore("  §eStats").setScore(7);
    ob.getScore("  §fKills§8: §e" + config2.getInt("Player." + p.getName() + ".Kills")).setScore(6);
    ob.getScore("  §fDeaths§8: §e" + config2.getInt("Player." + p.getName() + ".Deaths")).setScore(5);
    ob.getScore("  §fLevel§8: §e" + config2.getInt("Player." + p.getName() + ".Level")).setScore(4);
    if ((config2.getInt("Config." + p.getName() + ".Kills") > 0) && (config2.getInt("Config." + p.getName() + ".Deaths") > 0))
    {
      double kdr = config2.getInt("Config." + p.getName() + ".Kills") / config2.getInt("Config." + p.getName() + ".Deaths");
      ob.getScore("  §fKDR§8: §e" + kdr).setScore(3);
      ob.getScore("  §fCredits§8: §e" + config2.getInt("Player." + p.getName() + ".Credits")).setScore(2);
      ob.getScore("  §fOnline§8: §e" + Bukkit.getOnlinePlayers().size() + "/20").setScore(1);
      ob.getScore("§1").setScore(0);
      p.setScoreboard(board);
    }
    else
    {
      ob.getScore("  §fKDR§8: §e0.0").setScore(3);
      ob.getScore("  §fCredits§8: §e" + config2.getInt("Player." + p.getName() + ".Credits")).setScore(2);
      ob.getScore("  §fOnline§8: §e" + Bukkit.getOnlinePlayers().size() + "/20").setScore(1);
      ob.getScore("§1").setScore(0);
      p.setScoreboard(board);
    }
  }
  
  public static void TDMScorebord(Player p)
  {
    Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
    Objective ob = board.registerNewObjective("sb", "dummy");
    
    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    
    ob.setDisplaySlot(DisplaySlot.SIDEBAR);
    
    ob.setDisplayName("§e§nTDM");
    
    ob.getScore("§0").setScore(12);
    ob.getScore("  §eStats").setScore(11);
    ob.getScore("  §fKills§8: §e" + config2.getInt("Player." + p.getName() + ".GKills")).setScore(10);
    ob.getScore("  §fDeaths§8: §e" + config2.getInt("Player." + p.getName() + ".GDeaths")).setScore(9);
    if ((config2.getInt("Config." + p.getName() + ".GKills") > 0) && (config2.getInt("Config." + p.getName() + ".GDeaths") > 0))
    {
      double kdr = config2.getInt("Config." + p.getName() + ".GKills") / config2.getInt("Config." + p.getName() + ".GDeaths");
      ob.getScore("  §fKDR§8: §e" + kdr).setScore(8);
      ob.getScore("  §fCredits§8: §e" + config2.getInt("Player." + p.getName() + ".Credits")).setScore(7);
      ob.getScore("  §fOnline§8: §e" + Bukkit.getOnlinePlayers().size() + "/20").setScore(6);
      ob.getScore("  §fTime left§8: §e" + config.getInt("Config.GameTime")).setScore(5);
      ob.getScore("§1").setScore(4);
      ob.getScore("  §eTeams").setScore(3);
      ob.getScore("  §fRed§8: §e" + config.getInt("Config.Red")).setScore(2);
      ob.getScore("  §fBlue§8: §e" + config.getInt("Config.Blue")).setScore(1);
      ob.getScore("§2").setScore(0);
      p.setScoreboard(board);
    }
    else
    {
      ob.getScore("  §fKDR§8: §e0.0").setScore(8);
      ob.getScore("  §fCredits§8: §e" + config2.getInt("Player." + p.getName() + ".Credits")).setScore(7);
      ob.getScore("  §fOnline§8: §e" + Bukkit.getOnlinePlayers().size() + "/20").setScore(6);
      ob.getScore("  §fTime left§8: §e" + config.getInt("Config.GameTime")).setScore(5);
      ob.getScore("§1").setScore(4);
      ob.getScore("  §eTeams").setScore(3);
      ob.getScore("  §fRed§8: §e" + config.getInt("Config.Red")).setScore(2);
      ob.getScore("  §fBlue§8: §e" + config.getInt("Config.Blue")).setScore(1);
      ob.getScore("§2").setScore(0);
      p.setScoreboard(board);
    }
  }
}
