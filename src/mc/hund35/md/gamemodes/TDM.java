package mc.hund35.md.gamemodes;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mc.hund35.md.main;
import mc.hund35.md.listeners.guns;

public class TDM
  implements Listener
{
  public static void loadGame(Player p)
  {
    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    if ((config.getBoolean("Config.Game") == true) && (p.getWorld().getName().equals("world")))
    {
      File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
      if (config2.getString("Player." + p.getName() + ".Team").equals("Red"))
      {
        p.getInventory().clear();
        p.updateInventory();
        ItemStack item = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta im = (LeatherArmorMeta)item.getItemMeta();
        im.setColor(Color.RED);
        item.setItemMeta(im);
        
        ItemStack item2 = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta im2 = (LeatherArmorMeta)item2.getItemMeta();
        im2.setColor(Color.RED);
        item2.setItemMeta(im2);
        
        ItemStack item3 = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta im3 = (LeatherArmorMeta)item3.getItemMeta();
        im3.setColor(Color.RED);
        item3.setItemMeta(im3);
        
        ItemStack item4 = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta im4 = (LeatherArmorMeta)item4.getItemMeta();
        im4.setColor(Color.RED);
        item4.setItemMeta(im);
        p.getInventory().setHelmet(item);
        p.getInventory().setChestplate(item2);
        p.getInventory().setLeggings(item3);
        p.getInventory().setBoots(item4);
        ItemStack knife = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta meta = knife.getItemMeta();
        meta.setDisplayName("§eKnife");
        knife.setItemMeta(meta);
        p.getInventory().setItem(0, knife);
        p.updateInventory();
        guns.Slot1(p);
        World world = Bukkit.getServer().getWorld("games");
        p.teleport(new Location(world, -1145.0D, 63.0D, 404.585D));
      }
      else if (config2.getString("Player." + p.getName() + ".Team").equals("Blue"))
      {
        p.getInventory().clear();
        p.updateInventory();
        ItemStack item = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta im = (LeatherArmorMeta)item.getItemMeta();
        im.setColor(Color.BLUE);
        item.setItemMeta(im);
        
        ItemStack item2 = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        LeatherArmorMeta im2 = (LeatherArmorMeta)item2.getItemMeta();
        im2.setColor(Color.BLUE);
        item2.setItemMeta(im2);
        
        ItemStack item3 = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        LeatherArmorMeta im3 = (LeatherArmorMeta)item3.getItemMeta();
        im3.setColor(Color.BLUE);
        item3.setItemMeta(im3);
        
        ItemStack item4 = new ItemStack(Material.LEATHER_BOOTS, 1);
        LeatherArmorMeta im4 = (LeatherArmorMeta)item4.getItemMeta();
        im4.setColor(Color.BLUE);
        item4.setItemMeta(im);
        
        p.getInventory().setHelmet(item);
        p.getInventory().setChestplate(item2);
        p.getInventory().setLeggings(item3);
        p.getInventory().setBoots(item4);
        ItemStack knife = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta meta = knife.getItemMeta();
        meta.setDisplayName("§eKnife");
        knife.setItemMeta(meta);
        p.getInventory().setItem(0, knife);
        p.updateInventory();
        guns.Slot1(p);
        
        World world = Bukkit.getServer().getWorld("games");
        p.teleport(new Location(world, -950.0D, 63.0D, 385.684D));
      }
    }
  }
  
  public static void teamFinder(Player p)
  {
    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((!config.getBoolean("Config.Game")) && (p.getWorld().getName().equals("world"))) {
      switch (config.getInt("Config.Players"))
      {
      case 0: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(1));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 1: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(2));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 2: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(3));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 3: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(4));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 4: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(5));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 5: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(6));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 6: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(7));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 7: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(8));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 8: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(9));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 9: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(10));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 10: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(11));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 11: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(12));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 12: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(13));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 13: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(14));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 14: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(15));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 15: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(16));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 16: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(17));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 17: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(18));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 18: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Red", Integer.valueOf(config.getInt("Config.Red") + 1));
          config.set("Config.Players", Integer.valueOf(19));
          config2.set("Player." + p.getName() + ".Team", "Red");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      case 19: 
        if (config2.getString("Player." + p.getName() + ".Team").equals("§cnon"))
        {
          config.set("Config.Blue", Integer.valueOf(config.getInt("Config.Blue") + 1));
          config.set("Config.Players", Integer.valueOf(20));
          config2.set("Player." + p.getName() + ".Team", "Blue");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
          try
          {
            config2.save(file2);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
        break;
      }
    }
  }
  
  public static void endGame(Player p)
  {
    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((!config.getBoolean("Config.Game")) && (p.getWorld().getName().equals("games")))
    {
      if (config2.getString("Player." + p.getName() + ".Team").equals("Red"))
      {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvtp " + p.getName() + " world");
        p.getInventory().clear();
        ItemStack air = new ItemStack(Material.AIR);
        p.getInventory().setHelmet(air);
        p.getInventory().setChestplate(air);
        p.getInventory().setLeggings(air);
        p.getInventory().setBoots(air);
        p.updateInventory();
        ItemStack menu = new ItemStack(Material.NETHER_STAR, 1);
        ItemMeta meta = menu.getItemMeta();
        meta.setDisplayName("§eGun menu");
        menu.setItemMeta(meta);
        p.getInventory().setItem(0, menu);
        p.updateInventory();
        config2.set("Player." + p.getName() + ".Team", "§cnon");
        config2.set("Player." + p.getName() + ".GKills", Integer.valueOf(0));
        config2.set("Player." + p.getName() + ".GDeaths", Integer.valueOf(0));
        try
        {
          config2.save(file2);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
      if (config2.getString("Player." + p.getName() + ".Team").equals("Blue"))
      {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvtp " + p.getName() + " world");
        p.getInventory().clear();
        ItemStack air = new ItemStack(Material.AIR);
        p.getInventory().setHelmet(air);
        p.getInventory().setChestplate(air);
        p.getInventory().setLeggings(air);
        p.getInventory().setBoots(air);
        p.updateInventory();
        ItemStack menu = new ItemStack(Material.NETHER_STAR, 1);
        ItemMeta meta = menu.getItemMeta();
        meta.setDisplayName("§eGun menu");
        menu.setItemMeta(meta);
        p.getInventory().setItem(0, menu);
        p.updateInventory();
        config2.set("Player." + p.getName() + ".Team", "§cnon");
        config2.set("Player." + p.getName() + ".GKills", Integer.valueOf(0));
        config2.set("Player." + p.getName() + ".GDeaths", Integer.valueOf(0));
        try
        {
          config2.save(file2);
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
  }
  
  @EventHandler
  public void Respawn(PlayerRespawnEvent event)
  {
    Player p = event.getPlayer();
    File file = new File(main.instance.getDataFolder().getPath(), "data/nextgame.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if (config.getBoolean("Config.Game"))
    {
      if (config2.getString("Player." + p.getName() + ".Team").equals("Red"))
      {
        p.sendMessage("red");
        p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 312, 1));
        World world = Bukkit.getServer().getWorld("games");
        event.setRespawnLocation(new Location(world, -1145.0D, 63.0D, 404.585D));
        ItemStack ammo1 = new ItemStack(Material.CLAY_BALL, 64);
        p.getInventory().setItem(17, ammo1);
        p.updateInventory();
      }
      if (config2.getString("Player." + p.getName() + ".Team").equals("Blue"))
      {
        p.sendMessage("blue");
        p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 312, 1));
        World world = Bukkit.getServer().getWorld("games");
        event.setRespawnLocation(new Location(world, -950.0D, 63.0D, 385.681D));
        ItemStack ammo1 = new ItemStack(Material.CLAY_BALL, 64);
        p.getInventory().setItem(17, ammo1);
        p.updateInventory();
      }
    }
    else
    {
      World world = Bukkit.getServer().getWorld("world");
      event.setRespawnLocation(new Location(world, 4404.0D, 111.0D, 1966.607D));
    }
  }
  
  
  
  @EventHandler
  public void onDamage(EntityDamageByEntityEvent event)
  {
	  if (event.getEntity() instanceof Player && event.getDamager() instanceof Player){
	  
    Player damager = (Player)event.getDamager();
    LivingEntity damaged = (LivingEntity)event.getEntity();
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + damaged.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + damager.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((config.getString("Player." + damaged.getName() + ".Team").equals("Red")) && (config2.getString("Player." + damager.getName() + ".Team").equals("Red")))
    {
      event.setCancelled(true);
      return;
    }
    if ((config.getString("Player." + damaged.getName() + ".Team").equals("Blue")) && (config2.getString("Player." + damager.getName() + ".Team").equals("Blue")))
    {
      event.setCancelled(true);
      return;
    }
  }
  }
  
  @EventHandler
  public void onShotDamage(EntityDamageByEntityEvent event)
  {
	  if (event.getEntity() instanceof Player && event.getDamager() instanceof Player){
		  
	  
    Snowball arrow = (Snowball)event.getDamager();
    Player damager = (Player)arrow.getShooter();
    LivingEntity damaged = (LivingEntity)event.getEntity();
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + damaged.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + damager.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((config.getString("Player." + damaged.getName() + ".Team").equals("Red")) && (config2.getString("Player." + damager.getName() + ".Team").equals("Red")))
    {
      event.setCancelled(true);
      return;
    }
    else if ((config.getString("Player." + damaged.getName() + ".Team").equals("Blue")) && (config2.getString("Player." + damager.getName() + ".Team").equals("Blue")))
    {
      event.setCancelled(true);
      return;
    }
  }
  }
  
  @EventHandler
  public void DeathEvent(PlayerDeathEvent event)
  {
	  if (event.getEntity() instanceof Player && event.getEntity().getKiller() instanceof Player){
    Player k = event.getEntity().getKiller();
    Player d = event.getEntity();
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + k.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + d.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if (config.getString("Player." + k.getName() + ".Team").equals("Red"))
    {
      k.sendMessage("§c" + k.getName() + " §7killed §9" + d.getName() + " §7+ §e25 XP §7& §e$1");
      config.set("Player." + k.getName() + ".Kills", Integer.valueOf(config.getInt("Player." + k.getName() + ".Kills") + 1));
      config.set("Player." + k.getName() + ".GKills", Integer.valueOf(config.getInt("Player." + k.getName() + ".GKills") + 1));
      config.set("Player." + k.getName() + ".Credits", Integer.valueOf(config.getInt("Player." + k.getName() + ".Credits") + 1));
      config.set("Player." + k.getName() + ".XP", Integer.valueOf(config.getInt("Player." + k.getName() + ".XP") + 25));
      try
      {
        config.save(file);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    if (config.getString("Player." + k.getName() + ".Team").equals("Blue"))
    {
      k.sendMessage("§9" + k.getName() + " §7killed §c" + d.getName() + " §7+ §e25 XP §7& §e$1");
      config.set("Player." + k.getName() + ".Kills", Integer.valueOf(config.getInt("Player." + k.getName() + ".Kills") + 1));
      config.set("Player." + k.getName() + ".GKills", Integer.valueOf(config.getInt("Player." + k.getName() + ".GKills") + 1));
      config.set("Player." + k.getName() + ".Credits", Integer.valueOf(config.getInt("Player." + k.getName() + ".Credits") + 1));
      config.set("Player." + k.getName() + ".XP", Integer.valueOf(config.getInt("Player." + k.getName() + ".XP") + 25));
      try
      {
        config.save(file);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    d.sendMessage("§cKilled by: §7" + k.getName() + "§c!");
    config2.set("Player." + d.getName() + ".Deaths", Integer.valueOf(config2.getInt("Player." + d.getName() + ".Deaths") + 1));
    config2.set("Player." + d.getName() + ".GDeaths", Integer.valueOf(config2.getInt("Player." + d.getName() + ".GDeaths") + 1));
    try
    {
      config2.save(file2);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    event.setDeathMessage(null);
  }
  }
}
